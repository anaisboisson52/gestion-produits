# Gestion des Produits

Ce README fournit des instructions pour la gestion des produits via la conteneurisation de l'application et l'utilisation de Docker Compose.

## 1. Conteneurisation de l'application

### Prérequis
- Docker doit être installé sur votre machine.
- Cette application est compatible PHP5 et a été testée avec une base de données MySQL 5.7.

### Étapes de conteneurisation

1. Build de l'image PHP :
   ```bash
   docker build -t gestion-produits-php -f Dockerfile-php .
   ```

2. Build de l'image MySql :
    ```bash
    docker build -t gestion-produits-mysql -f Dockerfile-mysql .
    ```

3. Création d'un réseau Docker  :
    ```bash
   docker network create reseau_docker
    ```

4. Run de l'image PHP  :
    ```bash
   docker run -it --name PHP_5.0 --network reseau_docker -p 8080:80  gestion-produits-php
    ```

5. Run de l'image MySql  :
    ```bash
   docker run -d -it --name MySQL_5.7 --network reseau_docker -p 33060:3306 -v mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=password  gestion-produits-mysql
    ```


## 2. Mise en place de Docker Compose
```bash
    docker-compose up 
```


## 3. Version de dev : mise à jour de la plate-forme
- Une nouvelle branche a été créé pour toutes les modifications : 
```bash
    nom de la branche : partie_3
```

- Le fichier docker-compose a été modifié pour prendre en compte les nouvelles version de PHP et MySQL. 
- Création d'un nouveau dockerfile pour php 8.3
- Création d'un nouveau dockerfile pour mysql 8.3

Pour tester les modification, il suffit de faire la commande :
```bash
    docker-compose up 
```
